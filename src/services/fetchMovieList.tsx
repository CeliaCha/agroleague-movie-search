const API_KEY = '9e421810'

type Params = {
  title: string;
  year: string;
  type: string;
}

const fetchMovieList = (params: Params) => {
  const titleQuery = params.title ? `&s=${params.title}` : ''
  const yearQuery = params.year ? `&y=${params.year}` : ''
  const typeQuery = params.type ? `&type=${params.type}` : ''
  const queryParams = titleQuery + yearQuery + typeQuery

  return fetch(`http://www.omdbapi.com/?apikey=${API_KEY}${queryParams}`)
    .then((resp) => resp)
    .then((resp) => resp.json())
    .then((response) => {
      if (response.Response === 'False') {
        return { error: response.Error, data: null }
      }
      return { error: null, data: response.Search }
    })
    .catch(({ message }) => ({ error: message, data: null }))
}

export default fetchMovieList
