const API_KEY = '9e421810'

const fetchMovieDetail = (id: string) =>
  fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&i=${id}`)
    .then((resp) => resp)
    .then((resp) => resp.json())
    .then((response) => {
      if (response.Response === 'False') {
        return { error: response.Error, data: null }
      }
      console.log(response)
      return { error: null, data: response }
    })
    .catch(({ message }) => ({ error: message, data: null }))

export default fetchMovieDetail
