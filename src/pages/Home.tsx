import React, { useEffect, useState, useCallback } from 'react'
import SearchForm from '../components/SearchForm'
import MovieList from '../components/MovieList'
import fetchMovieList from '../services/fetchMovieList'

type MovieData = {
  Title: string;
  imdbID: string;
  Poster: string;
  Type: string;
}[]

function Home() {
  const [data, setData] = useState<MovieData | null>(null)
  const [error, setError] = useState<string | null>(null)
  const [query, setQuery] = useState({
    title: '',
    year: '',
    type: '',
  })

  const callback = useCallback((params) => {
    setQuery(params)
  }, [])

  useEffect(() => {
    fetchMovieList(query).then((response) => {
      setError(response.error)
      setData(response.data)
    })
  }, [query])

  return (
    <>
      <SearchForm parentCallback={callback} />
      {data && <MovieList data={data} error={error} />}
    </>
  )
}

export default Home
