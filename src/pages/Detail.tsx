import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'

import fetchMovieDetail from '../services/fetchMovieDetail'

type Movie = {
    Title: string;
    imdbID: string;
    Poster: string;
    Plot: string;
    Actors: string;
    Genre: string;
    Director: string;
    Released: string;
}

function Detail() {
  const { id }: { id:string;} = useParams()
  const [movie, setMovie] = useState<Movie | null>(null)
  const [error, setError] = useState<String | null>(null)

  useEffect(() => {
    fetchMovieDetail(id).then((response) => {
      setMovie(response.data)
      setError(response.error)
    })
  }, [id])

  return (
    <>
      {error !== null && <div>{error}</div>}

      {movie && (
        <article key={movie.imdbID} className="moviearticle">
          <h1 className="moviearticle__title">{movie.Title}</h1>
          <img
            src={movie.Poster}
            alt="Movie poster"
            className="moviearticle__image"
          />
          <p className="moviearticle__plot">{movie.Plot}</p>
          <p className="moviearticle__actors">Acteurs : {movie.Actors}</p>
          <p className="moviearticle__genre">Genre : {movie.Genre}</p>
          <p className="moviearticle__director">
            Réalisateur(s) : {movie.Director}
          </p>
          <p className="moviearticle__released">
            Date de sortie : {movie.Released}
          </p>
        </article>
      )}
    </>
  )
}

export default Detail
