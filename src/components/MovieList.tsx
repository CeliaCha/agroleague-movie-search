import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

type MovieListProps = {
  error: string | null;
  data: {
    Title: string;
    imdbID: string;
    Poster: string;
    Type: string;
  }[];
}

function MovieList({ error, data }: MovieListProps) {
  return (
    <ul className="movielist">
      {error !== null && <div>{error}</div>}
      {data &&
        data.map((item) => (
          <li key={item.imdbID} className="movieitem">
            <Link to={`/movie/${item.imdbID}`}>
            <img
                src={item.Poster}
                alt="Movie poster"
                className="movieitem__image"
              />
              <h1 className="movieitem__title">{item.Title}</h1>

            </Link>
          </li>
        ))}
    </ul>
  )
}

MovieList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      // id: PropTypes.number.isRequired,
      title: PropTypes.string,
    }).isRequired,
  ),
  error: PropTypes.string,
}
export default MovieList
