import React from 'react'
import PropTypes from 'prop-types'

import { useInput } from '../hooks/input-hook'

interface SearchFormProps {
  parentCallback: Function;
}

function SearchForm({ parentCallback }: SearchFormProps) {
  const { value: title, bind: bindTitle, reset: resetTitle } = useInput('')
  const { value: year, bind: bindYear, reset: resetYear } = useInput('')
  const { value: type, bind: bindType, reset: resetType } = useInput('')

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    parentCallback({ title, year, type })
  }
  return (
    <form className="searchform" onSubmit={handleSubmit}>
      <label>
        Titre: 
        <input required type="text" {...bindTitle} />
      </label>
      <label>
        Année: 
        <input type="text" {...bindYear} />
      </label>
      <label>
        Type:
        <select {...bindType}>
          <option value="movie">Film</option>
          <option value="series">Série</option>
          <option value="episode">Episode</option>
        </select>
      </label>
      <input type="submit" value="Lancer la recherche" className="submitbutton"/>
    </form>
  )
}

SearchForm.propTypes = {
  parentCallback: PropTypes.func.isRequired,
}

export default SearchForm
