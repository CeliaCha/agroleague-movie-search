import React from 'react'
import { Route, BrowserRouter as Router } from 'react-router-dom'
import Header from './components/Header'
import Home from './pages/Home'
import Detail from './pages/Detail'

import './App.scss'

export const App = () => (
  <div className="app">
    <Router>
      <Header />
      <Route exact path="/" component={Home} />
      <Route path="/movie/:id" component={Detail} />
    </Router>
  </div>
)

export default App
